SELECT communities.id,communities.name AS community,permissions.name AS permission ,COUNT(*) AS count_users
FROM community_member_permissions 
	LEFT JOIN community_members
		ON community_member_permissions.member_id = community_members.id
	LEFT JOIN communities
		ON communities.id = community_members.community_id
	LEFT JOIN permissions
		ON permissions.id = community_member_permissions.permission_id

GROUP BY communities.id,permissions.name
HAVING count_users>3
ORDER BY communities.id DESC, count_users ASC
LIMIT 100
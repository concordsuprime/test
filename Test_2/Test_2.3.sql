SELECT users.name AS name, communities.name AS community,permissions.name AS permission
FROM community_member_permissions
	LEFT JOIN community_members
		ON community_member_permissions.member_id = community_members.id
	LEFT JOIN communities
		ON community_members.community_id = communities.id 
	LEFT JOIN permissions
		ON permissions.id = community_member_permissions.permission_id 
	LEFT JOIN users
		ON users.id = community_members.user_id
		
WHERE (users.name LIKE '%T%' OR users.name LIKE '%t%' OR permissions.name LIKE '%articles') AND LENGTH(communities.name)>=15





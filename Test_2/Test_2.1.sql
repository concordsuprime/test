SELECT users.name AS user_name, communities.name AS community_name,community_members.joined_at
FROM community_members 
LEFT OUTER JOIN users
ON community_members.user_id = users.id

LEFT OUTER JOIN communities
ON community_members.community_id = communities.id 
WHERE community_members.community_id IN (
	SELECT id
	FROM communities 
	WHERE communities.created_at >= '2013-01-01 00:00:00' )
ORDER BY community_members.joined_at ASC





<?php

use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 13.08.2020
 * Time: 19:43
 */

class HelperTest extends TestCase
{
    private $helper;

    protected function setUp(): void
    {
        $this->helper = new \App\Helper();
    }

    protected function tearDown(): void
    {

    }

    /**
     * @dataProvider helperProvider
     */
    public function testHighlight($textInput,$textOutput){
        $this->assertEquals($textOutput, $this->helper->highlight_nicknames($textInput));
    }

    public function helperProvider(){
        return [
            ['@storm87 сообщил нам вчера о результатах','<b>@storm87</b> сообщил нам вчера о результатах'],
            ['Я живу в одном доме с @300spartans','Я живу в одном доме с @300spartans'],
            ['Правильный ник: @usernick | неправильный ник: @usernick;','Правильный ник: <b>@usernick</b> | неправильный ник: @usernick;'],
            ['Правильный ник: @usernick @usernick | неправильный ник: @usernick;','Правильный ник: <b>@usernick</b> <b>@usernick</b> | неправильный ник: @usernick;'],
            ['Неправильный ник: @usernick@usernick & неправильный ник: @usernick;','Неправильный ник: @usernick@usernick & неправильный ник: @usernick;'],
        ];
    }
}
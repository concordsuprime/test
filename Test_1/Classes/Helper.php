<?php
namespace App;


class Helper {

    private $text;


    public function __construct(string $text=null){
        $this->text=$text;
    }


    /**Highlight a nickname in the text
     *
     * @param string $text
     * @return string
     */
    public function highlight_nicknames(string $text){

        //Старался сделать максимальное использование регулярного выражения

        /* Так как пробелы в начале и конце заняты. То 2 ника подряд не могут стоять,
        Но условие не говорит о том что ник может быть один, следовательно пришлось придумывать как это обойти
        Смысл в том, что я заменяю все слова что нашел, и пробелы остаются в тексте, это можно сказать
        "разблокируют" следующие слова, и так будем делать в цикле пока все слова не найдем.
        */
        $reg = '/(\s|^)@[a-zA-Z][a-zA-Z0-9]{2,30}(\s|$)/';

        while (preg_match($reg,$text)){
            preg_match_all($reg,$text,$matches,PREG_OFFSET_CAPTURE);
            foreach ($matches[0] as $arrNik){
                $highlight_nickname = ' <b>'.trim($arrNik[0]).'</b> ';
                $text = preg_replace('/'.$arrNik[0].'/',$highlight_nickname,$text);
            }
        }


        return trim($text);

    }
}
